#!/usr/bin/env python

from __future__ import print_function
from functools import partial

import re
import argparse
import hpccm
import os
import itertools
from hpccm.building_blocks import *
from hpccm.primitives import *


def generateRecipeAwave(baseImage):
    Stage0 = hpccm.Stage()

    # Start "Recipe"
    Stage0 += baseimage(image=baseImage)

    if 'centos' in baseImage:
        Stage0 += shell(commands=['curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash'])

    Stage0 += packages(ospackages=['git-lfs'], epel=True)

    Stage0 += shell(commands=['git lfs install'])

    Stage0 += packages(
        apt=[
            'libglib2.0-dev',
            'python'
            ],
        yum=[
            'glib2-devel',
            'python2'
            ], epel=True)

    Stage0 += pip(pip='pip3')

    # Stage0 += generic_autotools(prefix='/opt/madagascar',
    #                             repository='https://github.com/ahay/src.git')
    Stage0 += generic_autotools(prefix='/opt/madagascar',
                                url='https://sourceforge.net/projects/rsf/files/madagascar/madagascar-3.0/madagascar-3.0.1.tar.gz',
                                directory='madagascar-3.0',
                                preconfigure=['rm -rf ./user/*']
                                )

    Stage0 += environment(variables={'RSF_ROOT': '/opt/madagascar'})

    hpccm.config.set_container_format('docker')

    return Stage0


def generateRecipeBeso(baseImage):
    Stage0 = hpccm.Stage()

    # Start "Recipe"
    Stage0 += baseimage(image=baseImage)

    if 'centos' in baseImage:
        Stage0 += shell(commands=['curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash'])

    # Install needed packages
    Stage0 += packages(
        apt=[
            'libboost-all-dev',
            'lcov',
            'php',
            'php-xml',
            'xsltproc',
            'python'
            ],
        yum=[
            'boost-devel',
            'lcov',
            'php',
            'php-xml',
            'libxslt',
            'python2'
            ], epel=True)

    # pip install --upgrade pip
    # pip install gcovr
    Stage0 += pip(upgrade=True, packages=['gcovr'])

    Stage0 += catalyst(edition='Base-Essentials-Extras', version='5.5.2',
                       prefix='/opt/catalyst')

    hpccm.config.set_container_format('docker')

    return Stage0

def generateRecipePlasma(baseImage):
    Stage0 = hpccm.Stage()

    # Start "Recipe"
    Stage0 += baseimage(image=baseImage)

    if 'centos' in baseImage:
        Stage0 += shell(commands=['curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash'])

    # Install Lapack
    Stage0 += comment('Lapack installation')
    Stage0 += generic_cmake(cmake_opts=['-DCMAKE_BUILD_TYPE=Release',
                                        '-DBUILD_SHARED_LIBS=ON',
                                        '-DBUILD_TESTING=ON',
                                        '-DCBLAS=ON',
                                        '-DLAPACKE=ON',
                                        '-DLAPACKE_WITH_TMG=ON'],
                            preconfigure=['export OMP_NUM_THREADS=1'],
                            directory='lapack-3.9.0',
                            url='https://github.com/Reference-LAPACK/lapack/archive/v3.9.0.tar.gz')

    hpccm.config.set_container_format('docker')

    return Stage0

def generateRecipeSlate(baseImage):
    Stage0 = hpccm.Stage()

    # Start "Recipe"
    Stage0 += baseimage(image=baseImage)

    if 'centos' in baseImage:
        Stage0 += shell(commands=['curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash'])

    # Install Lapack
    Stage0 += comment('Lapack installation')
    Stage0 += generic_cmake(cmake_opts=['-DCMAKE_BUILD_TYPE=Release',
                                        '-DBUILD_SHARED_LIBS=ON',
                                        '-DBUILD_TESTING=ON',
                                        '-DCBLAS=ON',
                                        '-DLAPACKE=ON',
                                        '-DLAPACKE_WITH_TMG=ON'],
                            preconfigure=['export OMP_NUM_THREADS=1'],
                            directory='lapack-3.9.0',
                            url='https://github.com/Reference-LAPACK/lapack/archive/v3.9.0.tar.gz')

    Stage0 += comment('Update cmake')
    Stage0 += cmake(eula=True, version='3.20.0')

    Stage0 += comment('ScaLapack installation')
    Stage0 += generic_cmake(url='http://www.netlib.org/scalapack/scalapack-2.1.0.tgz')
    

    hpccm.config.set_container_format('docker')

    return Stage0


def combine(list1, list2):
    return map(lambda x: '-'.join(x), list(itertools.product(list1, list2)))


def main():
    print("Generating App image definitions...")

    # list of combinations of configurations that can be used
    allConf = ["ubuntu18.04", "centos7"]
    allConf += combine(allConf, ["cuda9.2", "cuda10.1", "cuda10.2"])
    allConf = combine(allConf, ["mpich", "mpich-legacy",
                                "openmpi", "openmpi-legacy",
                                "mvapich2"])

    outputFolder = "Dockerfiles"

    for currConf in allConf:
        baseImageDev = "ompcluster/runtime:" + currConf
        baseImageExp = "ompcluster/runtime-dev:" + currConf

        # Generation of Dockerfile for Awave
        defFileTextAwaveDev = str(generateRecipeAwave(baseImageDev))
        defFileTextAwaveExp = str(generateRecipeAwave(baseImageExp))
        defFileNameAwaveDev = "awave-dev-" + currConf
        defFileNameAwaveExp = "awave-exp-" + currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileNameAwaveDev}", "w") as text_file:
            text_file.write(str(defFileTextAwaveDev))
        with open(f"{outputFolder}/{defFileNameAwaveExp}", "w") as text_file:
            text_file.write(str(defFileTextAwaveExp))

        # Generation of Dockerfile for BESO
        defFileTextBesoDev = str(generateRecipeBeso(baseImageDev))
        defFileTextBesoExp = str(generateRecipeBeso(baseImageExp))
        defFileNameBesoDev = "beso-dev-" + currConf
        defFileNameBesoExp = "beso-exp-" + currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileNameBesoDev}", "w") as text_file:
            text_file.write(str(defFileTextBesoDev))
        with open(f"{outputFolder}/{defFileNameBesoExp}", "w") as text_file:
            text_file.write(str(defFileTextBesoExp))

        # Generation of Dockerfile for PLASMA
        defFileTextPlasmaDev = str(generateRecipePlasma(baseImageDev))
        defFileTextPlasmaExp = str(generateRecipePlasma(baseImageExp))
        defFileNamePlasmaDev = "plasma-dev-" + currConf
        defFileNamePlasmaExp = "plasma-exp-" + currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileNamePlasmaDev}", "w") as text_file:
            text_file.write(str(defFileTextPlasmaDev))
        with open(f"{outputFolder}/{defFileNamePlasmaExp}", "w") as text_file:
            text_file.write(str(defFileTextPlasmaExp))

        # Generation of Dockerfile for SLATE
        defFileTextSlateDev = str(generateRecipeSlate(baseImageDev))
        defFileTextSlateExp = str(generateRecipeSlate(baseImageExp))
        defFileNameSlateDev = "slate-dev-" + currConf
        defFileNameSlateExp = "slate-exp-" + currConf

        # save container definition file
        with open(f"{outputFolder}/{defFileNameSlateDev}", "w") as text_file:
            text_file.write(str(defFileTextSlateDev))
        with open(f"{outputFolder}/{defFileNameSlateExp}", "w") as text_file:
            text_file.write(str(defFileTextSlateExp))


if __name__ == "__main__":
    main()
